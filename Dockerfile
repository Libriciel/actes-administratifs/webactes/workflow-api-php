FROM hubdocker.libriciel.fr/php:8.3.10-cli-alpine3.20 AS base


RUN <<EOF
apk add php83-pdo_pgsql bash bash-completion git php83-pecl-pcov jq curl

cp /usr/lib/php83/modules/* /usr/local/lib/php/extensions/no-debug-non-zts-20230831/
docker-php-ext-enable intl opcache pdo_pgsql pcov

mkdir /etc/bash_completion.d/
./bin/console completion bash > /etc/bash_completion.d/console
ln -s /app/bin/console /usr/local/bin/
EOF

COPY --from=hubdocker.libriciel.fr/composer:2.7.4 /usr/bin/composer /usr/bin/composer


ARG UID=10001
ARG GID=10001

RUN addgroup -S appgroup -g $GID && adduser -S appuser -G appgroup -u $UID

USER appuser

WORKDIR /app

COPY --chown=appuser:appgroup . /app/

RUN  composer install

CMD ["/bin/bash"]

