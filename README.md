# Utilisation

```
composer require libriciel/workflow-api-php
```

Voir les exemples dans le repertoires `exemples`. 

# Développement

Voir le Makefile


# Biographie

- Utilisation de PSR-7, 17 et 18 : https://github.com/KnpLabs/php-github-api/tree/master/lib/Github/Api