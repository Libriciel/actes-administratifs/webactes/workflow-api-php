<?php

use Http\Discovery\Psr18Client;
use WorkflowClient\Api\ArchiveApi;
use WorkflowClient\Api\DefinitionApi;
use WorkflowClient\Api\InstanceApi;
use WorkflowClient\Client;
use WorkflowClient\Model\GetHistoricInstancesQuery;
use WorkflowClient\Model\GetInstancesQuery;
use WorkflowClient\Model\GetSpecificInstancesQuery;
use WorkflowClient\Model\Instance;
use WorkflowClient\Model\InstanceSearchRequest;
use WorkflowClient\Model\SearchInstancesQuery;
use WorkflowClient\Model\State;
use WorkflowClient\Model\Visibility;

require_once __DIR__ . '/../vendor/autoload.php';
$httpClient = new Psr18Client();

$client = Client::createWithHttpClient($httpClient, 'http://workflow:8080');

$tenantId = 'libriciel';
$metadataId = 'nb_jour_conge';
$bureauId = 'direction_du_developpement';
$workflowKey = 'visa_conge';
$user = 'eric';


$archiveApi = new ArchiveApi($client);

$getHistoricInstanceQuery = new GetHistoricInstancesQuery();
$getHistoricInstanceQuery->tenantId = $tenantId;


$historicInstanceList = $archiveApi->getHistoricInstances($getHistoricInstanceQuery);
print_r($historicInstanceList);

$historicInstance = $archiveApi->getHistoricInstance($tenantId, $historicInstanceList->data[0]->id);
print_r($historicInstance);

$archiveApi->deleteHistoricInstance( $historicInstanceList->data[0]->id);

$historicInstanceList = $archiveApi->getHistoricInstances($getHistoricInstanceQuery);
print_r($historicInstanceList);
