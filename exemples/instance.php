<?php

use Http\Discovery\Psr18Client;
use WorkflowClient\Api\DefinitionApi;
use WorkflowClient\Api\InstanceApi;
use WorkflowClient\Client;
use WorkflowClient\Model\GetInstancesQuery;
use WorkflowClient\Model\GetSpecificInstancesQuery;
use WorkflowClient\Model\Instance;
use WorkflowClient\Model\InstanceSearchRequest;
use WorkflowClient\Model\SearchInstancesQuery;
use WorkflowClient\Model\State;
use WorkflowClient\Model\Visibility;

require_once __DIR__ . '/../vendor/autoload.php';
$httpClient = new Psr18Client();

$client = Client::createWithHttpClient($httpClient, 'http://workflow:8080');

$tenantId = 'libriciel';
$metadataId = 'nb_jour_conge';
$bureauId = 'direction_du_developpement';
$workflowKey = 'visa_conge';
$user = 'eric';

$definitionApi = new DefinitionApi($client);


$result = $definitionApi->getWorkflowDefinitionByKey($tenantId, $workflowKey);
$workflowId = $result->id;
$workflowKey = $result->key;
echo sprintf("Récupération de l'id du workflow : %s\n", $workflowId);

$instanceApi = new InstanceApi($client);
$instance = new Instance();
//$instance->id = 'monid2';
$instance->tenantId = $tenantId;
$instance->state = State::DRAFT;
$instance->validationWorkflowId = 'null';
$instance->creationWorkflowId = 'null';
$instance->name = 'Ma demande de congé 2222';
$instance->visibility = Visibility::PUBLIC;
$instance->businessKey = 'identifiant_du_dossier';
$instance->originGroup = $bureauId;
$instance->originUser = 'eric';
$instance->finalGroup = $bureauId;

//$instance->variables = ['CECIESTMAVARIABLE' => '42'];

$createdInstance = $instanceApi->createInstance($instance);
echo "Instance crée : {$createdInstance->id}\n";

$getSpecificInstancesQuery = new GetSpecificInstancesQuery();
$getSpecificInstancesQuery->tenantId = $tenantId;
$getSpecificInstancesQuery->withHistory = false;
$instanceRecup = $instanceApi->getInstance($createdInstance->id, $getSpecificInstancesQuery);
$instanceRecup->name  = 'nouveau nom';



$instanceApi->updateInstance($tenantId, $createdInstance->id, $instanceRecup);
echo "ok";




$getInstancesQuery = new GetInstancesQuery();
$getInstancesQuery->tenantId = $tenantId;
$all = $instanceApi->getInstances($getInstancesQuery);
print_r($all);

$getSpecificInstancesQuery = new GetSpecificInstancesQuery();
$getSpecificInstancesQuery->tenantId = $tenantId;
$getSpecificInstancesQuery->withHistory = true;
$result = $instanceApi->getInstance($createdInstance->id, $getSpecificInstancesQuery);
print_r($result);

$searchInstanceQuery = new SearchInstancesQuery();
$searchInstanceQuery->tenantId = $tenantId;

$instanceSearchRequest = new InstanceSearchRequest();
$instanceSearchRequest->state = State::DRAFT;
$paginatedTask = $instanceApi->searchInstances($searchInstanceQuery, $instanceSearchRequest);
print_r($paginatedTask);

echo "Liste instance :\n";
$listInstance = $instanceApi->getSpecificInstances($getSpecificInstancesQuery, [$createdInstance->id]);
print_r($listInstance);

echo "readTask : \n";
$readTasks = $instanceApi->getReadTasks($createdInstance->id);
print_r($readTasks);

echo "historyTask : \n";
$historyTask = $instanceApi->getHistoryTasks($createdInstance->id);
print_r($historyTask);

echo "get instance by metadatavalue : \n";
$getInstancesByMetadataValueQuery = new \WorkflowClient\Model\GetInstancesByMetadataValueQuery();
$getInstancesByMetadataValueQuery->tenantId = $tenantId;
$getInstancesByMetadataValueQuery->metadataKey = 'nb_jour_conge';

$taskList = $instanceApi->getInstancesByMetadataValue($getInstancesByMetadataValueQuery);
print_r($taskList);

foreach ($all->data as $task) {
    $instanceApi->deleteInstance($task->instanceId);
}

$createdInstance = $instanceApi->createInstance($instance);
echo "Instance crée : {$createdInstance->id}\n";
