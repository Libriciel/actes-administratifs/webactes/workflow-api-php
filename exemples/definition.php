<?php

use Http\Discovery\Psr18Client;
use Symfony\Component\Uid\Uuid;
use WorkflowClient\Api\DefinitionApi;
use WorkflowClient\Client;
use WorkflowClient\Model\Action;
use WorkflowClient\Model\CreateWorkflowDefinitionQuery;
use WorkflowClient\Model\GetWorkflowDefinitionsByGroupIdQuery;
use WorkflowClient\Model\GetWorkflowDefinitionsByMetadataQuery;
use WorkflowClient\Model\GetWorkflowDefinitionsQuery;
use WorkflowClient\Model\ParallelType;
use WorkflowClient\Model\StepDefinition;
use WorkflowClient\Model\WorkflowDefinition;

require_once __DIR__ . '/../vendor/autoload.php';
$httpClient = new Psr18Client();

$client = Client::createWithHttpClient($httpClient, 'http://workflow:8080');

$tenantId = 'libriciel';
$metadataId = 'nb_jour_conge';
$bureauId = 'direction_du_developpement';
$workflowKey = 'visa_conge';

$definitionApi = new DefinitionApi($client);

$definitionQuery = new CreateWorkflowDefinitionQuery();

$step1 = new StepDefinition();
$step1->type = Action::VISA;
$step1->parallelType = ParallelType::OR;
$step1->mandatoryValidationMetadataIds = [$metadataId];
$step1->validatingDeskIds = [$bureauId];



$workflowDefinition = new WorkflowDefinition();
$workflowDefinition->name = $workflowKey;
$workflowDefinition->key = $workflowKey;
$workflowDefinition->id = $workflowKey;
$workflowDefinition->steps = [$step1];
$workflowDefinition->finalDeskId = '${i_Parapheur_internal_emitter_id}';
$workflowDefinition->finalNotifiedDeskIds = [];


echo "Création du workflow\n";
$definitionApi->createWorkflowDefinition($tenantId, $workflowDefinition);
echo "Récupération du workflow\n";
$result = $definitionApi->getWorkflowDefinitionByKey($tenantId, $workflowKey);
print_r($result);


echo "Mise à jour du workflow\n";
$workflowDefinition->name = "Nouveau nom du workflow";
$definitionApi->updateWorkflowDefinition($tenantId, $workflowKey, $workflowDefinition);

echo "Récupération par la clé \n";
$result = $definitionApi->getWorkflowDefinitionByKey($tenantId, $workflowKey);
print_r($result);

echo "Récupération par l'id \n";
$workflowDefinitionResult = $definitionApi->getWorkflowDefinitionById($tenantId, $result->id);
print_r($workflowDefinitionResult);

echo "Récupération par id du bureau \n";
$getWorkflowDefinitionByMetadataQuery = new GetWorkflowDefinitionsByGroupIdQuery();
$getWorkflowDefinitionByMetadataQuery->tenantId = $tenantId;

$result = $definitionApi->getWorkflowDefinitionsByGroupId(
    $bureauId,
    $getWorkflowDefinitionByMetadataQuery
);
print_r($result);

echo "Récupération par métadonnées \n";
$getWorkflowDefinitionByMetadataQuery = new GetWorkflowDefinitionsByMetadataQuery();
$getWorkflowDefinitionByMetadataQuery->tenantId = $tenantId;

$result = $definitionApi->getWorkflowDefinitionsByMetadata(
    $metadataId,
    $getWorkflowDefinitionByMetadataQuery
);
print_r($result);


echo "Suppression du workflow\n";
$definitionApi->deleteWorkflowDefinition($tenantId, $workflowKey);

echo "Liste des workflows\n";
$getWorflowDefinitionQuery = new GetWorkflowDefinitionsQuery();
$getWorflowDefinitionQuery->tenantId = $tenantId;
$worflowList = $definitionApi->getWorkflowDefinitions($getWorflowDefinitionQuery);
print_r($worflowList);

echo "Création du workflow\n";
$definitionApi->createWorkflowDefinition($tenantId, $workflowDefinition);
