<?php

use Http\Discovery\Psr18Client;
use WorkflowClient\Api\DefinitionApi;
use WorkflowClient\Api\InstanceApi;
use WorkflowClient\Api\TaskApi;
use WorkflowClient\Client;
use WorkflowClient\Model\Action;
use WorkflowClient\Model\GetInstancesQuery;
use WorkflowClient\Model\GetSpecificInstancesQuery;
use WorkflowClient\Model\Instance;
use WorkflowClient\Model\InstanceSearchRequest;
use WorkflowClient\Model\PerformTaskRequest;
use WorkflowClient\Model\SearchInstancesQuery;
use WorkflowClient\Model\State;
use WorkflowClient\Model\Visibility;

require_once __DIR__ . '/../vendor/autoload.php';
$httpClient = new Psr18Client();

$client = Client::createWithHttpClient($httpClient, 'http://workflow:8080');

$tenantId = 'libriciel';
$metadataId = 'nb_jour_conge';
$bureauId = 'direction_du_developpement';
$workflowKey = 'visa_conge';
$user = 'eric';
$definitionApi = new DefinitionApi($client);
$instanceApi = new InstanceApi($client);

$getInstancesQuery = new GetInstancesQuery();
$getInstancesQuery->tenantId = $tenantId;
$all = $instanceApi->getInstances($getInstancesQuery);
print_r($all);
$task = $all->data[0];
$instanceId = $all->data[0]->instanceId;
echo "Récupération de l'id de l'instance : $instanceId\n";


$getSpecificInstancesQuery = new GetSpecificInstancesQuery();
$getSpecificInstancesQuery->tenantId = $tenantId;
$myInstance = $instanceApi->getInstance($instanceId, $getSpecificInstancesQuery);
//print_r($myInstance);

echo "taskId = {$task->id}\n";
echo "taskId = {$myInstance->taskList[0]->id}\n";


$taskApi = new TaskApi($client);


$workflowId = $task->variables['workflow_internal_validation_workflow_id'];
/*echo "WorkflowId : $workflowId\n";
echo "Liste des workflows\n";
$workflow = $definitionApi->getWorkflowDefinitionById($tenantId, $workflowId);
print_r($workflow);*/



echo "Liste des tache\n";

$taskResult = $taskApi->getTask($task->id);
print_r($taskResult);



echo "Perform tache\n";
$performTaskRequest = new PerformTaskRequest();

$performTaskRequest->action = Action::START;
$performTaskRequest->userId = $user;
$performTaskRequest->groupId = $bureauId;
$performTaskRequest->delegatedByGroupId = null;

$taskApi->performTask($task->id, $performTaskRequest);



$getSpecificInstancesQuery = new GetSpecificInstancesQuery();
$getSpecificInstancesQuery->tenantId = $tenantId;
$myInstance = $instanceApi->getInstance($instanceId, $getSpecificInstancesQuery);
print_r($myInstance);

$performTaskRequest->action = Action::VISA;
$taskApi->performTask($myInstance->taskList[0]->id, $performTaskRequest);


$getSpecificInstancesQuery = new GetSpecificInstancesQuery();
$getSpecificInstancesQuery->tenantId = $tenantId;
$myInstance = $instanceApi->getInstance($instanceId, $getSpecificInstancesQuery);
print_r($myInstance);


$performTaskRequest->action = Action::ARCHIVE;
$taskApi->performTask($myInstance->taskList[0]->id, $performTaskRequest);

$getSpecificInstancesQuery = new GetSpecificInstancesQuery();
$getSpecificInstancesQuery->tenantId = $tenantId;
$myInstance = $instanceApi->getInstance($instanceId, $getSpecificInstancesQuery);
print_r($myInstance);
/*$getSpecificInstancesQuery = new GetSpecificInstancesQuery();
$getSpecificInstancesQuery->tenantId = $tenantId;
$getSpecificInstancesQuery->withHistory = true;
$result = $instanceApi->getInstance($instanceId, $getSpecificInstancesQuery);
print_r($result);*/