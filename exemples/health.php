<?php

use Http\Discovery\Psr18Client;
use WorkflowClient\Api\DefinitionApi;
use WorkflowClient\Api\GroupApi;
use WorkflowClient\Api\HealthApi;
use WorkflowClient\Api\InstanceApi;
use WorkflowClient\Api\TaskApi;
use WorkflowClient\Client;
use WorkflowClient\Model\Action;
use WorkflowClient\Model\FilteredRequest;
use WorkflowClient\Model\FolderFilter;
use WorkflowClient\Model\GetInstancesQuery;
use WorkflowClient\Model\GetSpecificInstancesQuery;
use WorkflowClient\Model\Instance;
use WorkflowClient\Model\InstanceSearchRequest;
use WorkflowClient\Model\PerformTaskRequest;
use WorkflowClient\Model\SearchInstancesQuery;
use WorkflowClient\Model\State;
use WorkflowClient\Model\Visibility;

require_once __DIR__ . '/../vendor/autoload.php';
$httpClient = new Psr18Client();

$client = Client::createWithHttpClient($httpClient, 'http://workflow:8080');


$healthApi = new HealthApi($client);

$result = $healthApi->health();
print_r($result);
