
curl -X 'POST' \
  'http://workflow:8080/workflow/definitions?tenantId=libriciel' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{"id":"visa_conge","key":"visa_conge","name":"visa_conge","steps":[{"type":"VISA","validatingDeskIds":["direction_du_developpement"],"notifiedDeskIds":[],"mandatoryValidationMetadataIds":["nb_jour_conge"],"mandatoryRejectionMetadataIds":[],"parallelType":"OR"}],"finalDeskId":"direction_du_developpement","finalNotifiedDeskIds":[]}'

workflowId=$(curl -X 'GET' \
  'http://workflow:8080/workflow/definitions/byKey/visa_conge?tenantId=libriciel' \
  -H 'accept: application/json' | jq -r ".id")

instanceId=$(curl -X 'POST' \
  'http://workflow:8080/workflow/instance' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{"id":"id3","name":"Ma demande de cong\u00e9 2222","legacyId":null,"businessKey":"identifiant_du_dossier","tenantId":"libriciel","variables":{"CECIESTMAVARIABLE":"42"},"state":"DRAFT","originGroup":"direction_du_developpement","originUser":"eric","finalGroup":"direction_du_developpement","validationWorkflowId":"'.$workflowId.'","visibility":"PUBLIC"}' \
  | jq -r ".id")

curl -X 'GET' \
  "http://workflow:8080/workflow/instance/$instanceId?tenantId=libriciel" \
  -H 'accept: application/json'

taskId=$(curl -X 'GET'   "http://workflow:8080/workflow/instance/$instanceId?tenantId=libriciel" -H 'accept: application/json' | jq -r ".taskList[0].id")

curl -X 'POST' \
  "http://workflow:8080/workflow/task/$taskId" \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{"action":"START","userId":"eric","groupId":"direction_du_developpement"}'
