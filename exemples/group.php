<?php

use Http\Discovery\Psr18Client;
use WorkflowClient\Api\DefinitionApi;
use WorkflowClient\Api\GroupApi;
use WorkflowClient\Api\InstanceApi;
use WorkflowClient\Api\TaskApi;
use WorkflowClient\Client;
use WorkflowClient\Model\Action;
use WorkflowClient\Model\FilteredRequest;
use WorkflowClient\Model\FolderFilter;
use WorkflowClient\Model\GetInstancesQuery;
use WorkflowClient\Model\GetSpecificInstancesQuery;
use WorkflowClient\Model\Instance;
use WorkflowClient\Model\InstanceSearchRequest;
use WorkflowClient\Model\PerformTaskRequest;
use WorkflowClient\Model\SearchInstancesQuery;
use WorkflowClient\Model\State;
use WorkflowClient\Model\Visibility;

require_once __DIR__ . '/../vendor/autoload.php';
$httpClient = new Psr18Client();

$client = Client::createWithHttpClient($httpClient, 'http://workflow:8080');

$tenantId = 'Libriciel';
$metadataId = 'nb_jour_conge';
$bureauId = 'direction_du_developpement';
$workflowKey = 'visa_conge';
$user = 'eric';
$definitionApi = new DefinitionApi($client);
$instanceApi = new InstanceApi($client);
$groupApi  = new GroupApi($client);


$count = $groupApi->countWorkflows($bureauId, State::PENDING);
print_r($count);

$filterringParameter = new \WorkflowClient\Model\FilteringParameter();
$filterringParameter->groupId = $bureauId;
$filterringParameter->state = null;
$filteredRequest = new FilteredRequest();
$filteredRequest->filteringParameters = [$filterringParameter];
$filteredRequest->folderFilter = null;

$filteringResult = $groupApi->requestCounts($filteredRequest);
print_r($filteringResult);

$folderFilter = new FolderFilter();
$folderFilter->metadataFilters = [ $metadataId => ''];

$filteredRequest->folderFilter = $folderFilter;
$filteringResult = $groupApi->requestTasks($bureauId, State::PENDING, null, $filteredRequest);
print_r($filteringResult);
