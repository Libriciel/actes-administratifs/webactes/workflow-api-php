# workflow-api-php


## 0.4.2 - 2024-12-26

### Évolutions

- Ajout de la traduction des états

## 0.4.1 - 2024-12-20

## Corrections

- Fix serialisation des enum

## 0.4.0 - 2024-09-16

### Évolutions

- passage à Symfony 7.1

## 0.3.1 - 2024-08-27

### Évolutions

- ajout de la fonction updateInstance

## 0.3.0 - 2024-08-23

### Évolutions

- remplacement de l'api getWorkflowDefinition par getWorkflowDefinitions pour permettre la pagination

## 0.2.3 - 2024-08-23

### Évolutions

- ajout de l'api health

## 0.2.2

### Corrections

- modification du composer

## 0.2.1

### Évolutions

- passage en php 8.3

## 0.1.1

### Évolutions

- une version plus fonctionnelle

## 0.1.0

### Évolutions

- création initiale
