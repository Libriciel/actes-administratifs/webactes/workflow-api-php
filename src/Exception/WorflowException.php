<?php

declare(strict_types=1);

namespace WorkflowClient\Exception;

use Http\Client\Exception;

final class WorflowException extends \Exception implements Exception
{
}
