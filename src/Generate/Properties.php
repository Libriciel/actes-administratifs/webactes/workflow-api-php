<?php

namespace WorkflowClient\Generate;

class Properties
{
    public string $name;
    public string $type;
    public ?string $subType = null;
}
