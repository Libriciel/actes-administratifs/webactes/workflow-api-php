<?php

namespace WorkflowClient\Model;

class GetWorkflowDefinitionsQuery
{
    public int $page;
    public int $pageSize;

    public string $tenantId;

    public SortByGetWorkflowDefinition $sortby;

    public bool $asc;

    public string $searchTerm;
}
