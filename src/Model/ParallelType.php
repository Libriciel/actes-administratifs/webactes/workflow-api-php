<?php

namespace WorkflowClient\Model;

enum ParallelType : string
{
    case OR = 'OR';
    case AND = 'AND';
}