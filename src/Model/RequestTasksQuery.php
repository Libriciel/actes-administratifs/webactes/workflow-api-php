<?php

namespace WorkflowClient\Model;

class RequestTasksQuery
{
    public SortBy $sortBy;
    public bool $asc;
    public int $page;
    public int $pageSize;
}
