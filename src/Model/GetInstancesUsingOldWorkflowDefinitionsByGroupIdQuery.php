<?php

namespace WorkflowClient\Model;

class GetInstancesUsingOldWorkflowDefinitionsByGroupIdQuery
{
    public string $tenantId;
    public int $page;
    public int $size;
    /** @var string[] */
    public array $sort;
}
