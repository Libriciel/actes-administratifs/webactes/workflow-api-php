<?php

namespace WorkflowClient\Model;

class FolderFilter
{
    public string $typeId;
    public string $subtypeId;
    public int $createdAfter;
    public int $createdBefore;
    public string $searchData;
    public string $legacyId;
    /** @var string[] */
    public array $metadataFilters;
}