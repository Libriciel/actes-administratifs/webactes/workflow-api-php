<?php

namespace WorkflowClient\Model;

class InstanceSearchRequest
{
    /** @var string[] */
    public array $groupIds;
    public string $typeId;
    public string $subtypeId;
    public string $legacyId;
    public string $searchTerm;
    public int $emitBeforeTime;
    public int $stillSinceTime;
    public State $state;
}