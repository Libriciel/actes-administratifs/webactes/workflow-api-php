<?php

namespace WorkflowClient\Model;

class FilteredRequest
{
    /** @var FilteringParameter[] */
    public array $filteringParameters;
    public ?FolderFilter $folderFilter = null;
}