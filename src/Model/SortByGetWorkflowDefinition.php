<?php

namespace WorkflowClient\Model;

enum SortByGetWorkflowDefinition : string
{
    case NAME = 'NAME';
    case KEY = 'KEY';
    case ID = 'ID';
}