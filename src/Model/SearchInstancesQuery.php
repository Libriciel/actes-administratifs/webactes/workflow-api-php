<?php

namespace WorkflowClient\Model;

class SearchInstancesQuery
{
    public string $tenantId;
    public int $page;
    public int $pageSize;
    public SortBy $sortBy;
    public bool $asc;
}
