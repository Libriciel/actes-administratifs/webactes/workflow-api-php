<?php

namespace WorkflowClient\Model;

class PaginatedListTask
{
    /** @var Task[] */
    public array $data;
    public int $page;
    public int $pageSize;
    public int $total;
}