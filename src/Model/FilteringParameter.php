<?php

namespace WorkflowClient\Model;

class FilteringParameter
{
    public ?string $groupId;
    public ?State $state;
    public string $filterMetadataKey;
    public string $filterMetadataValue;
}