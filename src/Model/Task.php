<?php

namespace WorkflowClient\Model;

class Task
{
    public ?string $id;
    public ?string $legacyId;
    public ?string $instanceId;
    public ?string $instanceName;
    public ?Action $expectedAction;
    public ?Action $performedAction;
    public ?string $notificationType;
    public \Datetime $beginDate;
    public \Datetime $dueDate;
    public \Datetime $date;
    /** @var string[] */
    public array $candidateGroups;
    /** @var string[]|null */
    public ?array $notifiedGroups;
    /** @var string[]|null */
    public ?array $mandatoryValidationMetadata;
    /** @var string[]|null */
    public ?array $mandatoryRejectionMetadata;
    public ?string $assignee;
    public ?string $delegatedByGroupId;
    public bool $pending;
    /** @var string[]|null */
    public ?array $variables;
    public ?ExternalState $externalState;
    public ?State $state;
    /** @var string[]|null */
    public ?array $readByUserIds;
    public int $workflowIndex;
    public ?int $stepIndex;
    public ?Visibility $visibility;
    public string $originGroup;
    public string $finalGroup;
}