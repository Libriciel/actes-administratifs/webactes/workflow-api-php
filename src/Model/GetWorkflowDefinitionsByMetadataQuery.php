<?php

namespace WorkflowClient\Model;

class GetWorkflowDefinitionsByMetadataQuery
{
    public string $tenantId;
    public int $page;
    public int $size;
    /** @var string[] */
    public array $sort;
}
