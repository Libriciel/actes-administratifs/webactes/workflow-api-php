<?php

namespace WorkflowClient\Model;

class PerformTaskRequest
{
    public Action $action;
    public string $userId;
    public string $groupId;
    public ?string $delegatedByGroupId;
    /** @var string[] */
    public array $variables;
}