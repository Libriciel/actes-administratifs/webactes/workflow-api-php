<?php

namespace WorkflowClient\Model;

class Instance
{
    public string $id;
    public string $name;
    public ?string $legacyId = null;
    public string $businessKey;
    public string $tenantId;
    public \Datetime $startTime;
    public \Datetime $dueDate;
    public \Datetime $endTime;
    /** @var string[] */
    public array $variables;
    /** @var string[]|null */
    public ?array $readByUserIds;
    public ?State $state;
    /** @var Task[] */
    public array $taskList;
    public ?string $processDefinitionId;
    public string $originGroup;
    public ?string $originUser;
    public string $finalGroup;
    /** @var string[]|null */
    public ?array $finalNotifiedGroups;
    public ?string $creationWorkflowId;
    public ?string $validationWorkflowId;
    public Visibility $visibility;

    public string $deploymentKey = 'i_Parapheur_internal_folder';
}