<?php

namespace WorkflowClient\Model;

class StepDefinition
{
    public Action $type;
    /** @var string[] */
    public array $validatingDeskIds;
    /** @var string[] */
    public array $notifiedDeskIds = [];
    /** @var string[] */
    public array $mandatoryValidationMetadataIds = [];
    /** @var string[] */
    public array $mandatoryRejectionMetadataIds = [];
    public ParallelType $parallelType;
}