<?php

namespace WorkflowClient\Model;

class PageInstance
{
    public int $totalPages;
    public int $totalElements;
    public int $size;
    /** @var Instance[] */
    public array $content;
    public int $number;
    public SortObject $sort;
    public PageableObject $pageable;
    public int $numberOfElements;
    public bool $first;
    public bool $last;
    public bool $empty;
}