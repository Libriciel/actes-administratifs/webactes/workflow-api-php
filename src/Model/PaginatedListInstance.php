<?php

namespace WorkflowClient\Model;

class PaginatedListInstance
{
    /** @var Instance[] */
    public array $data;
    public int $page;
    public int $pageSize;
    public int $total;
}