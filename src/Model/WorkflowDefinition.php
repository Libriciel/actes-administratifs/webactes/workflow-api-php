<?php

namespace WorkflowClient\Model;

class WorkflowDefinition
{
    public string $id;
    public string $key;
    public int $version;
    public int $usageCount;
    public string $name;
    /** @var StepDefinition[]|null */
    public ?array $steps;
    public ?string $finalDeskId;
    /** @var string[]|null */
    public ?array $finalNotifiedDeskIds;
    public bool $suspended = true;
}