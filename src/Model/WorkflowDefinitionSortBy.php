<?php

namespace WorkflowClient\Model;

enum WorkflowDefinitionSortBy : string
{
    case NAME = 'NAME';
    case KEY = 'KEY';
    case ID = 'ID';
}