<?php

namespace WorkflowClient\Model;

class GetSpecificInstancesQuery
{
    public bool $withHistory;
    public string $tenantId;
}
