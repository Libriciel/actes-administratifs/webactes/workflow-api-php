<?php

namespace WorkflowClient\Model;

use Symfony\Contracts\Translation\TranslatorInterface;

enum State : string
{
    case DRAFT = 'DRAFT';
    case RETRIEVABLE = 'RETRIEVABLE';
    case FINISHED = 'FINISHED';
    case VALIDATED = 'VALIDATED';
    case REJECTED = 'REJECTED';
    case LATE = 'LATE';
    case PENDING = 'PENDING';
    case DOWNSTREAM = 'DOWNSTREAM';
    public function trans(TranslatorInterface $translator, ?string $locale = null): string
    {
        return match ($this) {
            self::DRAFT => 'brouillon',
            self::RETRIEVABLE => 'récupérable',
            self::FINISHED => 'terminé',
            self::VALIDATED => 'validé',
            self::REJECTED => 'rejeté',
            self::LATE => 'en retard',
            self::PENDING => 'en cours',
            self::DOWNSTREAM => 'downstream',
        };
    }
}