<?php

namespace WorkflowClient\Model;

class PaginatedListWorkflowDefinition
{
    /** @var WorkflowDefinition[] */
    public array $data;
    public int $page;
    public int $pageSize;
    public int $total;
}