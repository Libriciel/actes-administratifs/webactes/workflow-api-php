<?php

namespace WorkflowClient\Model;

class GetInstancesQuery
{
    public string $tenantId;
    public int $page;
    public int $pageSize;
    public SortBy $sortBy;
    public bool $asc;
    public string $groupId;
    public string $typeId;
    public string $subtypeId;
    public string $legacyId;
    public string $searchTerm;
    public int $emitBeforeTime;
    public int $stillSinceTime;
    public State $state;
}