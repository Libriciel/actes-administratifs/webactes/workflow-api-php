<?php

namespace WorkflowClient\Model;

enum SortBy : string
{
    case VALIDATION_START_DATE = 'VALIDATION_START_DATE';
    case CREATION_DATE = 'CREATION_DATE';
    case STILL_SINCE_DATE = 'STILL_SINCE_DATE';
    case LATE_DATE = 'LATE_DATE';
    case END_DATE = 'END_DATE';
    case TASK_ID = 'TASK_ID';
    case INSTANCE_ID = 'INSTANCE_ID';
    case INSTANCE_NAME = 'INSTANCE_NAME';
    case ACTION_TYPE = 'ACTION_TYPE';
    case METADATA = 'METADATA';
    case LAST_ACTION_DATE = 'LAST_ACTION_DATE';
}