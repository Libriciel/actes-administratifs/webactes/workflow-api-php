<?php

namespace WorkflowClient\Model;

class ErrorResponse
{
    public string $timestamp;
    public int $status;
    public string $error;
    public string $message;
    public string $path;
}