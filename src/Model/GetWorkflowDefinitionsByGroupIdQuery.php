<?php

namespace WorkflowClient\Model;

class GetWorkflowDefinitionsByGroupIdQuery
{
    public string $tenantId;
    public int $page;
    public int $size;
    /** @var string[] */
    public array $sort;
}
