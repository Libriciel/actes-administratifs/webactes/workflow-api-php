<?php

namespace WorkflowClient\Model;

enum ExternalState : string
{
    case FORM = 'FORM';
    case ACTIVE = 'ACTIVE';
    case SIGNED = 'SIGNED';
    case REFUSED = 'REFUSED';
    case EXPIRED = 'EXPIRED';
    case ERROR = 'ERROR';
}