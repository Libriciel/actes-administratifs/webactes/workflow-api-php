<?php

namespace WorkflowClient\Model;

class GetInstancesByMetadataValueQuery
{
    public string $tenantId;
    public string $metadataKey;
    public string $metadataValue;
    public int $page;
    public int $size;
    /** @var string[] */
    public array $sort;
}
