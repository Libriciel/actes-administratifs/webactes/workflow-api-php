<?php

namespace WorkflowClient\Model;

class SortObject
{
    public bool $empty;
    public bool $unsorted;
    public bool $sorted;
}