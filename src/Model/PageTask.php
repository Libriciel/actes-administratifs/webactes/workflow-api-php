<?php

namespace WorkflowClient\Model;

class PageTask
{
    public int $totalPages;
    public int $totalElements;
    public int $size;
    /** @var Task[] */
    public array $content;
    public int $number;
    public SortObject $sort;
    public PageableObject $pageable;
    public int $numberOfElements;
    public bool $first;
    public bool $last;
    public bool $empty;
}