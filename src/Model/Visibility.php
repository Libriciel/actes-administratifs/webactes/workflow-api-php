<?php

namespace WorkflowClient\Model;

enum Visibility : string
{
    case PUBLIC = 'PUBLIC';
    case CONFIDENTIAL = 'CONFIDENTIAL';
}