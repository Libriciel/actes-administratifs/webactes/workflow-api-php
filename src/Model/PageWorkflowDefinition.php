<?php

namespace WorkflowClient\Model;

class PageWorkflowDefinition
{
    public int $totalPages;
    public int $totalElements;
    public int $size;
    /** @var WorkflowDefinition[] */
    public array $content;
    public int $number;
    public SortObject $sort;
    public PageableObject $pageable;
    public int $numberOfElements;
    public bool $first;
    public bool $last;
    public bool $empty;
}