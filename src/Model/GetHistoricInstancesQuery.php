<?php

namespace WorkflowClient\Model;

class GetHistoricInstancesQuery
{
    public string $tenantId;
    public int $page;
    public int $pageSize;
    public bool $asc;
    public int $stillSinceDate;
}
