<?php

namespace WorkflowClient\Model;

class FilteringParameterCountResult
{
    public string $groupId;
    public State $state;
    public ?string $filterMetadataKey;
    public ?string $filterMetadataValue;
    public int $count;
}