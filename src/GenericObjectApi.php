<?php

namespace WorkflowClient;

use Http\Client\Exception;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\BackedEnumNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use WorkflowClient\Exception\WorflowException;

class GenericObjectApi
{
    public function __construct(
        private readonly Client $client
    ) {
    }


    protected function getPath(string $path, object $queryObject = null): string
    {
        if (null !== $queryObject) {
            $encoders = [new UrlEncoder()];
            $normalizers = [new BackedEnumNormalizer(), new ObjectNormalizer()];
            $serializer = (new Serializer($normalizers, $encoders));
            $queryPart = $serializer->serialize($queryObject, 'url');
            $path .= "?$queryPart";
        }
        return $path;
    }

    /**
     * @throws Exception
     * @throws WorflowException
     */
    protected function get(string $path, string $returnClassName, object $queryObject = null)
    {
        $path = $this->getPath($path, $queryObject);
        $result = $this->client->get($path);
        return $this->deserialize($result, $returnClassName);
    }

    private function deserialize(ResponseInterface $result, string $returnClassName): mixed
    {
        $deserializedResult = (new ResponseDeserializer())
            ->deserialize($result, $returnClassName);
        if (! $deserializedResult instanceof $returnClassName && ! is_array($deserializedResult)) {
            throw new WorflowException("Unexpected token response");
        }
        return $deserializedResult;
    }

    /**
     * @throws Exception
     */
    public function post(string $path, string $returnClassName = "", object $requestObject = null)
    {
        if ($requestObject) {
            $body = (new ResponseDeserializer())->serialize($requestObject);
        } else {
            $body = "";
        }
        $result = $this->client->post($path, [], $body);
        if ($returnClassName) {
            return $this->deserialize($result, $returnClassName);
        }
        return null;
    }

    public function postArray(string $path, string $returnClassName = "", array $requestArray = null): mixed
    {
        if ($requestArray) {
            $body = json_encode($requestArray, JSON_THROW_ON_ERROR);
        } else {
            $body = "";
        }

        $result = $this->client->post($path, [], $body);
        if ($returnClassName) {
            return $this->deserialize($result, $returnClassName);
        }
        return null;
    }

    public function put(string $path, string $returnClassName = "", object $requestObject = null): mixed
    {
        if ($requestObject) {
            $body = (new ResponseDeserializer())->serialize($requestObject);
        } else {
            $body = "";
        }

        $result = $this->client->put($path, [], $body);

        if ($returnClassName) {
            return $this->deserialize($result, $returnClassName);
        }
        return null;
    }

    /**
     * @throws Exception
     */
    protected function delete(string $path, object $queryObject = null): void
    {
        $path = $this->getPath($path, $queryObject);
        $this->client->delete($path);
    }


    /**
     * @throws Exception
     */
    protected function getRaw(string $path): ResponseInterface
    {
        return $this->client->get($path);
    }
}
