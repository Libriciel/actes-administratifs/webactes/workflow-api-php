<?php

declare(strict_types=1);

namespace WorkflowClient;

use Http\Client\Common\HttpMethodsClientInterface;
use Http\Client\Common\Plugin\AddHostPlugin;
use Http\Client\Common\Plugin\ContentTypePlugin;
use Http\Client\Common\Plugin\HeaderDefaultsPlugin;
use Http\Client\Exception;
use WorkflowClient\HttpClient\Builder;
use WorkflowClient\HttpClient\Plugin\ExceptionThrower;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ResponseInterface;

final class Client
{
    private const USER_AGENT = 'workflow-api-php';
    private Builder $httpClientBuilder;

    public function __construct(string $url = null, Builder $httpClientBuilder = null)
    {
        $this->httpClientBuilder = $httpClientBuilder ?: new Builder();
        $this->httpClientBuilder->addPlugin(
            new HeaderDefaultsPlugin([
                'User-Agent' => self::USER_AGENT,
            ])
        );
        $this->httpClientBuilder->addPlugin(
            new ContentTypePlugin()
        );
        $this->httpClientBuilder->addPlugin(
            new ExceptionThrower()
        );
        if ($url) {
            $this->setUrl($url);
        }
    }

    public static function createWithHttpClient(ClientInterface $httpClient, string $url = null): self
    {
        $builder = new Builder($httpClient);
        return new self($url, $builder);
    }

    private function getHttpClientBuilder(): Builder
    {
        return $this->httpClientBuilder;
    }

    public function getHttpClient(): HttpMethodsClientInterface
    {
        return $this->getHttpClientBuilder()->getHttpClient();
    }

    private function setUrl(string $url): void
    {
        $this->getHttpClientBuilder()->removePlugin(AddHostPlugin::class);
        $this->getHttpClientBuilder()->addPlugin(
            new AddHostPlugin($this->getHttpClientBuilder()->getUriFactory()->createUri($url))
        );
    }

    /**
     * @throws Exception
     */
    public function get(string $uri, array $headers = []): ResponseInterface
    {
        return $this->getHttpClient()->get($uri, $headers);
    }

    /**
     * @throws Exception
     */
    public function post(string $uri, array $headers = [], string $body = ""): ResponseInterface
    {
        return $this->getHttpClient()->post($uri, $headers, $body);
    }

    public function put(string $uri, array $headers = [], string $body = ""): ResponseInterface
    {
        return $this->getHttpClient()->put($uri, $headers, $body);
    }

    /**
     * @throws Exception
     */
    public function delete(string $uri, array $headers = [], string $body = ""): ResponseInterface
    {
        return $this->getHttpClient()->delete($uri, $headers, $body);
    }
}
