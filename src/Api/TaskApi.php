<?php

namespace WorkflowClient\Api;

use WorkflowClient\GenericObjectApi;
use WorkflowClient\Exception\IparapheurV5Exception;
use Psr\Http\Message\ResponseInterface;
use WorkflowClient\Model\PerformTaskRequest;
use WorkflowClient\Model\Task;

class TaskApi extends GenericObjectApi
{
    public function performTask(
        string $taskId,
        PerformTaskRequest $performTaskRequest,
    ): void {
        $path = sprintf(
            "/workflow/task/%s",
            $taskId
        );
        $this->post(
            path: $path,
            requestObject: $performTaskRequest
        );
    }

    public function getTask(
        string $taskId,
        bool $includeInstanceData = false,
    ): Task {
        $path = sprintf(
            "/workflow/task/%s?includeInstanceData=%s",
            $taskId,
            json_encode($includeInstanceData),
        );
        return $this->get(
            path: $path,
            returnClassName: Task::class
        );
    }
}
