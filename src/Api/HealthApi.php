<?php

namespace WorkflowClient\Api;

use WorkflowClient\GenericObjectApi;
use WorkflowClient\Model\Health;

class HealthApi extends GenericObjectApi
{
    public function health(): Health
    {
        $path = "/workflow/actuator/health";
        return $this->get(
            path: $path,
            returnClassName: Health::class,
        );
    }
}
