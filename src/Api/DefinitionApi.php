<?php

namespace WorkflowClient\Api;

use WorkflowClient\GenericObjectApi;
use WorkflowClient\Model\GetWorkflowDefinitionsQuery;
use WorkflowClient\Model\PaginatedListWorkflowDefinition;
use WorkflowClient\Model\WorkflowDefinition;
use WorkflowClient\Model\PageWorkflowDefinition;
use WorkflowClient\Model\DeleteWorkflowDefinitionQuery;
use WorkflowClient\Model\GetWorkflowDefinitionsByMetadataQuery;
use WorkflowClient\Model\GetWorkflowDefinitionsByGroupIdQuery;

class DefinitionApi extends GenericObjectApi
{
    public function createWorkflowDefinition(
        string $tenantId,
        WorkflowDefinition $workflowDefinition,
    ): WorkflowDefinition {
        $path = sprintf(
            "/workflow/definitions?tenantId=%s",
            $tenantId
        );
        return $this->post(
            path: $path,
            returnClassName: WorkflowDefinition::class,
            requestObject: $workflowDefinition
        );
    }

    public function updateWorkflowDefinition(
        string $tenantId,
        string $definitionKey,
        WorkflowDefinition $workflowDefinition,
    ): WorkflowDefinition {
        $path = sprintf(
            "/workflow/definitions/%s?tenantId=%s",
            $definitionKey,
            $tenantId
        );
        return $this->put(
            path: $path,
            returnClassName: WorkflowDefinition::class,
            requestObject: $workflowDefinition
        );
    }

    public function deleteWorkflowDefinition(
        string $tenantId,
        string $definitionKey,
    ): void {
        $path = sprintf(
            "/workflow/definitions/%s?tenantId=%s",
            $definitionKey,
            $tenantId
        );
         $this->delete($path);
    }

    public function getWorkflowDefinitions(
        GetWorkflowDefinitionsQuery $getWorkflowDefinitionsQuery
    ): PaginatedListWorkflowDefinition {
        $path = "/workflow/definitions";

        return $this->get(
            $path,
            PaginatedListWorkflowDefinition::class,
            $getWorkflowDefinitionsQuery
        );
    }

    public function getWorkflowDefinitionById(
        string $tenantId,
        string $definitionId,
    ): WorkflowDefinition {
        $path = sprintf(
            "/workflow/definitions/%s?tenantId=%s",
            $definitionId,
            $tenantId,
        );
        return $this->get($path, WorkflowDefinition::class);
    }

    public function getWorkflowDefinitionByKey(
        string $tenantId,
        string $definitionKey,
    ): WorkflowDefinition {
        $path = sprintf(
            "/workflow/definitions/byKey/%s?tenantId=%s",
            $definitionKey,
            $tenantId,
        );
        return $this->get($path, WorkflowDefinition::class);
    }

    public function getWorkflowDefinitionsByMetadata(
        string $metadataId,
        GetWorkflowDefinitionsByMetadataQuery $getWorkflowDefinitionsByMetadataQuery = null,
    ): PageWorkflowDefinition {
        $path = sprintf(
            "/workflow/definitions/byMetadata/%s",
            $metadataId
        );
        return $this->get($path, PageWorkflowDefinition::class, $getWorkflowDefinitionsByMetadataQuery);
    }

    public function getWorkflowDefinitionsByGroupId(
        string $groupId,
        GetWorkflowDefinitionsByGroupIdQuery $getWorkflowDefinitionsByGroupIdQuery = null,
    ): PageWorkflowDefinition {
        $path = sprintf(
            "/workflow/definitions/byGroupId/%s",
            $groupId
        );
        return $this->get($path, PageWorkflowDefinition::class, $getWorkflowDefinitionsByGroupIdQuery);
    }

    public function setupIParapheurDefinitions(): void
    {
        $path = sprintf(
            "/workflow/definitions/i-parapheur"
        );
        $this->post(
            path: $path
        );
    }
}
