<?php

namespace WorkflowClient\Api;

use WorkflowClient\GenericObjectApi;
use WorkflowClient\Exception\IparapheurV5Exception;
use Psr\Http\Message\ResponseInterface;
use WorkflowClient\Model\array;
use WorkflowClient\Model\FilteringParameterCountResult;
use WorkflowClient\Model\PaginatedListTask;
use WorkflowClient\Model\IntResponse;
use WorkflowClient\Model\FilteredRequest;
use WorkflowClient\Model\State;
use WorkflowClient\Model\RequestTasksQuery;

class GroupApi extends GenericObjectApi
{
    public function requestCounts(
        FilteredRequest $filteredRequest,
    ): array {
        $path = sprintf(
            "/workflow/group"
        );
          return $this->post(
              path: $path,
              returnClassName: FilteringParameterCountResult::class . "[]",
              requestObject: $filteredRequest
          );
    }
    public function requestTasks(
        string $groupId,
        State $state,
        ?RequestTasksQuery $requestTasksQuery,
        FilteredRequest $filteredRequest,
    ): PaginatedListTask {
        $path = sprintf(
            "/workflow/group/%s/%s",
            $groupId,
            $state->value
        );
        if (null !== $requestTasksQuery) {
            $path = $this->getPath($path, $requestTasksQuery);
        }

          return $this->post(
              path: $path,
              returnClassName: PaginatedListTask::class,
              requestObject: $filteredRequest
          );
    }
    public function countWorkflows(string $groupId, State $state,): IntResponse
    {
        $path = sprintf(
            "/workflow/group/%s/count/%s",
            $groupId,
            $state->value
        );
        return $this->get($path, IntResponse::class);
    }
}
