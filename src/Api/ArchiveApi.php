<?php

namespace WorkflowClient\Api;

use WorkflowClient\GenericObjectApi;
use WorkflowClient\Model\Instance;
use WorkflowClient\Model\PaginatedListInstance;
use WorkflowClient\Model\GetHistoricInstancesQuery;

class ArchiveApi extends GenericObjectApi
{
    public function getHistoricInstances(
        GetHistoricInstancesQuery $getHistoricInstancesQuery = null,
    ): PaginatedListInstance {
        $path = sprintf(
            "/workflow/archive"
        );
        return $this->get($path, PaginatedListInstance::class, $getHistoricInstancesQuery);
    }

    public function getHistoricInstance(
        string $tenantId,
        string $instanceId,
    ): Instance {
        $path = sprintf(
            "/workflow/archive/%s?tenantId=%s",
            $instanceId,
            $tenantId
        );
        return $this->get($path, Instance::class);
    }

    public function deleteHistoricInstance(string $instanceId): void
    {
        $path = sprintf(
            "/workflow/archive/%s",
            $instanceId
        );
         $this->delete($path);
    }
}
