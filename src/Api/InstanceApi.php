<?php

namespace WorkflowClient\Api;

use WorkflowClient\GenericObjectApi;
use WorkflowClient\Exception\IparapheurV5Exception;
use Psr\Http\Message\ResponseInterface;
use WorkflowClient\Model\GetInstancesQuery;
use WorkflowClient\Model\PaginatedListInstance;
use WorkflowClient\Model\PaginatedListTask;
use WorkflowClient\Model\array;
use WorkflowClient\Model\PageInstance;
use WorkflowClient\Model\PageTask;
use WorkflowClient\Model\Instance;
use WorkflowClient\Model\InstanceSearchRequest;
use WorkflowClient\Model\SearchInstancesQuery;
use WorkflowClient\Model\GetSpecificInstancesQuery;
use WorkflowClient\Model\GetInstancesUsingOldWorkflowDefinitionsByGroupIdQuery;
use WorkflowClient\Model\GetInstancesUsingOldWorkflowDefinitionsByMetadataIdQuery;
use WorkflowClient\Model\GetInstancesByMetadataValueQuery;
use WorkflowClient\Model\Task;

class InstanceApi extends GenericObjectApi
{
    public function createInstance(
        Instance $instance,
    ): Instance {
        $path = sprintf(
            "/workflow/instance"
        );
          return $this->post(
              path: $path,
              returnClassName: Instance::class,
              requestObject: $instance
          );
    }

    public function getInstance(
        string $instanceId,
        GetSpecificInstancesQuery $getSpecificInstancesQuery
    ): Instance {
        $path = sprintf(
            "/workflow/instance/%s",
            $instanceId
        );
        return $this->get($path, Instance::class, $getSpecificInstancesQuery);
    }

    public function getInstances(
        GetInstancesQuery $getInstancesQuery
    ): PaginatedListTask {
        $path = sprintf(
            "/workflow/instance"
        );
        return $this->get($path, PaginatedListTask::class, $getInstancesQuery);
    }

    public function deleteInstance(string $instanceId,): void
    {
        $path = sprintf(
            "/workflow/instance/%s",
            $instanceId
        );
        $this->delete($path);
    }

    public function searchInstances(
        SearchInstancesQuery $searchInstancesQuery,
        InstanceSearchRequest $instanceSearchRequest,
    ): PaginatedListTask {
        $path = $this->getPath('/workflow/instance/search', $searchInstancesQuery);
          return $this->post(
              path: $path,
              returnClassName: PaginatedListTask::class,
              requestObject: $instanceSearchRequest
          );
    }
    public function getSpecificInstances(
        GetSpecificInstancesQuery $getSpecificInstancesQuery,
        array $ids,
    ): ?array {
        $path = $this->getPath('/workflow/instance/list', $getSpecificInstancesQuery);

          return $this->postArray(
              path: $path,
              returnClassName: Instance::class . "[]",
              requestArray: $ids
          );
    }
    public function getReadTasks(string $instanceId,): array
    {
        $path = sprintf(
            "/workflow/instance/%s/readTasks",
            $instanceId
        );
        return $this->get($path, Task::class . '[]');
    }
    public function getHistoryTasks(string $instanceId,): array
    {
        $path = sprintf(
            "/workflow/instance/%s/historyTasks",
            $instanceId
        );
        return $this->get($path, Task::class . '[]');
    }

    public function getInstancesByMetadataValue(
        GetInstancesByMetadataValueQuery $getInstancesByMetadataValueQuery = null,
    ): PageTask {
        $path = sprintf(
            "/workflow/instance/byMetadata"
        );
        return $this->get($path, PageTask::class, $getInstancesByMetadataValueQuery);
    }

    public function getInstancesUsingOldWorkflowDefinitionsByGroupId(
        string $groupId,
        GetInstancesUsingOldWorkflowDefinitionsByGroupIdQuery $getInstance = null,
    ): PageInstance {
        $path = sprintf(
            "/workflow/instance/fromOldDefinitions/%s",
            $groupId
        );
        return $this->get($path, PageInstance::class, $getInstance);
    }
    public function getInstancesUsingOldWorkflowDefinitionsByMetadataId(
        string $metadataId,
        GetInstancesUsingOldWorkflowDefinitionsByMetadataIdQuery $getInstance = null,
    ): PageInstance {
        $path = sprintf(
            "/workflow/instance/fromOldDefinitions/byMetadata/%s",
            $metadataId
        );
        return $this->get($path, PageInstance::class, $getInstance);
    }

    public function updateInstance(string $tenantId, string $instanceid, Instance $instance): void
    {
        $path = sprintf(
            "/workflow/instance/%s?tenantId=%s",
            $instanceid,
            $tenantId
        );
        $this->put(
            path: $path,
            requestObject: $instance
        );
    }
}
