<?php

namespace WorkflowClient;

use Psr\Http\Message\ResponseInterface;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\BackedEnumNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use WorkflowClient\Exception\WorflowException;

class ResponseDeserializer
{
    private function getSerializer(): Serializer
    {
        $extractor = new PropertyInfoExtractor([], [
            new PhpDocExtractor(),
            new ReflectionExtractor()
        ]);
        $normalizers = [
            new ArrayDenormalizer(),
            new DateTimeNormalizer(),
            new BackedEnumNormalizer(),
            new ObjectNormalizer(
                null,
                null,
                null,
                $extractor
            ),
        ];
        return (new Serializer($normalizers, [new JsonEncoder()]));
    }

    /**
     * @throws WorflowException
     */
    public function deserialize(ResponseInterface $response, string $className): mixed
    {
        if (
            ! $response->hasHeader('Content-Type') ||
            ($response->getHeaderLine('Content-Type') !== 'application/json' &&
                ! preg_match('#json$#', $response->getHeaderLine('Content-Type'))
            )
        ) {
            throw new WorflowException("Response is not in json");
        }
        return $this->getSerializer()
            ->deserialize($response->getBody(), $className, 'json');
    }

    public function serialize(object $object): string
    {
        return $this->getSerializer()->serialize($object, "json");
    }
}
